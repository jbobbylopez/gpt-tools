# gpt-tools
Objectives Brainstorming:
- Text to Speech interface to help new readers follow along visually with audio.
- Point text to an arbitrary file (start with .txt, then pdf, html, etc)
- Create an interface for a custom chatbot (multi-user, multi-device)
- Create context sessions / discussions that can be continued in context 
- Make a multi-platform UI
- Be able to record audio (to translate speech to text)
- Highlight text being recited audibly as the text is being read.
- integrate with discord?


POC Scripts
- tts-poc.py # text2speech sandbox where initial things will be discovered


Make sure to have your OpenAI key in your session before using this script.
E.g.:
export OPENAI_API_KEY='your-api-key-here'
