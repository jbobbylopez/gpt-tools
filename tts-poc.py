from openai import OpenAI
from pathlib import Path

client = OpenAI()

# Read text from the file
#input_file_path = Path(__file__).parent / "input.txt"
input_file_path = "/tmp/text-to-read.txt"
with open(input_file_path, "r") as file:
    text_input = file.read()

with client.audio.speech.with_streaming_response.create(
    model="tts-1",
    voice="alloy",
    input=text_input,
) as response:
    response.stream_to_file("speech-output.mp3")
